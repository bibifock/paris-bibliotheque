import { configure } from '@storybook/svelte';

import '../static/bootstrap.min.css';
import '../static/global.css';

// automatically import all files ending in *.stories.js
configure(require.context('../src', true, /\.stories\.js$/), module);

