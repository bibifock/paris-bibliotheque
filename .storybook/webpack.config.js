const path = require('path');

module.exports = ({ config }) => {
  config.resolve.alias = {
    ...config.resolve.alias,
    ui: path.resolve(__dirname, '../src/ui'),
    app: path.resolve(__dirname, '../src/app'),
    utils: path.resolve(__dirname, '../src/utils'),
    routes: path.resolve(__dirname, '../src/routes')
  };
  // Return the altered config
  return config;
};
