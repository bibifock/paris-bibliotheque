const makeResponse = res => {
  const sendResponse = json => res.end(JSON.stringify(json));
  const sendError = (message) => sendResponse({ error: true, message });
  const sendSuccess = (res) => sendResponse({ success: true, ...res });

  return { sendError, sendSuccess };
};

export default makeResponse;
