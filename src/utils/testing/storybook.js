export const actionAlert = (funcName) =>
  ({ detail }) => alert(`on:${funcName} => ${JSON.stringify(detail)}`);

export const generateActions = (actions) => actions.reduce(
  (obj, key) => ({ ...obj, [key]: actionAlert(key) }),
  {}
);

export const simulateRequest = (result, time = 500) => (params) => new Promise(
  resolve => setTimeout(() => resolve(result || params), time)
);
