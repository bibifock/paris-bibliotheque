export function post (endpoint, data, options = {}) {
  return fetch(endpoint, {
    method: 'POST',
    credentials: 'include',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    },
    ...options
  }).then(r => r.json());
}

export function get (endpoint) {
  return fetch(endpoint).then(r => r.json());
}

export function put (endpoint, data) {
  return post(endpoint, data, { method: 'PUT' });
}

export function del (endpoint, data) {
  return post(endpoint, data, { method: 'DELETE' });
}

export default post;
