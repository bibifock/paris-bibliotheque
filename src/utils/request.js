import rp from 'request-promise';

export const saveCookies = (jar, url) => {
  const cookies = jar.getCookies(url);
  return JSON.stringify(cookies.map(cookie => cookie.toString()));
};

export const loadCookies = (jar, json, url) => {
  JSON.parse(json).forEach(cookieStr => jar.setCookie(cookieStr, url));
}

export default (options = {}) => {
  const jar = rp.jar();

  const request = rp.defaults({
    json: true,
    jar,
    ...options
  });

  request.loadCookies = (json, url) => loadCookies(jar, json, url);
  request.getCookies = (url) => saveCookies(jar, url);

  return request;
}
