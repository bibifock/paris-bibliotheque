export const BASE_URL = 'https://bibliotheques.paris.fr';

export const URL_ACCOUNT_RESUME = `${BASE_URL}/Default/Portal/Services/ILSClient.svc/RetrieveShortAccount`;
export const URL_SEARCH = `${BASE_URL}/Default/Portal/Recherche/Search.svc/Search`;
export const URL_LOGIN = `${BASE_URL}/Default/Portal/Recherche/logon.svc/logon`;
export const URL_BASKET = `${BASE_URL}/Default/Portal/Recherche/Search.svc/SearchUserBasket`;
export const URL_BASKET_ADD = `${BASE_URL}/Default/Portal/Recherche/OpenFind.svc/AddBasketItem`;
export const URL_BASKET_DEL = `${BASE_URL}/Default/Portal/Recherche/OpenFind.svc/DeleteBasketItem`;
export const URL_HOLDING = `${BASE_URL}/Default/Portal/Services/ILSClient.svc/GetHoldings`;
export const URL_SITES = `${BASE_URL}/Default/Portal/recherche/Search.svc/Suggest`
export const URL_LOANS = `${BASE_URL}/Default/Portal/Services/UserAccountService.svc/ListLoans`;

export const URL_ACTION_LOANS_RENEW = `${BASE_URL}/Default/Portal/Services/UserAccountService.svc/RenewLoans`;

