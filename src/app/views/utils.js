import sortedIndexBy from 'lodash/sortedIndexBy';

import { post, put, del } from 'utils/post';

/**
 * @param object holding
 *
 * @return Promise<object>
 */
export const getLibrariesStatus = (holding) => post('api/holding.json', holding)
  .then(({ holding }) => holding);

/**
 * @param array books
 * @param string uid
 *
 * @return number index uid position
 */
export const findBookByUid = (books, uid) => books.findIndex(b => b.uid === uid);

/**
 * @param object book
 *
 * @thrown error
 *
 * @return Promise<bool>
 */
export const basketSelect = (book) => {
  const { uid, inBasket } = book;
  const func = inBasket ? del : put;

  return func(
    '/api/basket.json',
    { uid }
  ).then(({ success, message }) => {
    if (!success) {
      throw Error(message);
    }

    return true;
  });
};

/**
 * @param object book
 * @param array baskets
 *
 * @return array baskets
 */
export function basketDel(book, baskets) {
  const index = findBookByUid(baskets, book.uid);
  baskets.splice(index, 1);

  return baskets;
}

/**
 * @param object book
 * @param array baskets
 *
 * @return array baskets
 */
export function basketAdd(book, baskets) {
  const index = sortedIndexBy(baskets, book, '_sorted');
  baskets.splice(index, 0, { ...book, inBasket: true });

  getLibrariesStatus(book.holding)
    .then(libraries => {
      const index = findBookByUid(baskets, book.uid);
      if (index > -1) {
        baskets[index].libraries = libraries;
      }
    });

  return baskets;
}
