import * as ajax from 'utils/post';

import { basketAdd, basketDel } from './utils';

describe('[app/views] utils', () => {
  describe('basketAdd', () => {
    beforeEach(() => {
      jest.spyOn(ajax, 'post').mockResolvedValue({});
    });

    afterEach(() => {
      ajax.post.mockRestore();
    });

    it('should add book in basket', () => {
      const book = { uid: 'uid-to-add', inBasket: false, title: 'title' };
      const oldBaskets = [
          { uid: 'uid-1', inBasket: true, title: 'title-1' },
          { uid: 'uid-2', inBasket: true, title: 'title-2' }
      ];

      const baskets = basketAdd(book, [...oldBaskets]);

      expect(baskets).toHaveLength(3);
      expect(baskets[0]).toEqual({ ...baskets[0], inBasket: true });
    });
  });

  describe('basketDel', () => {
    it('should del book of basket', () => {
      const book = { uid: 'uid-to-add', inBasket: true, title: 'title' };

      const oldBaskets = [
        { uid: 'uid-1', inBasket: true, title: 'title-1' },
        book,
        { uid: 'uid-2', inBasket: true, title: 'title-2' }
      ];

      const baskets = basketDel(book, [...oldBaskets]);

      expect(baskets).toHaveLength(2);
      expect(baskets).toEqual([oldBaskets[0], oldBaskets[2]]);
    });
  });
});
