import { writable } from 'svelte/store';

export const baskets = writable([]);
export const loans = writable([]);

export const libraries = writable([]);
export const library = writable(false);
