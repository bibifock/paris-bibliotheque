import get from 'lodash/get';
import Url from 'url';

import getRequest from 'utils/request';
import makeResponse from 'utils/makeResponse';

import {
  URL_BASKET,
  URL_BASKET_ADD,
  URL_BASKET_DEL,
  BASE_URL
} from 'utils/urls';

const regSaga = /class="tarzan linktype-link"[^>]+href="(?<link>[^"]+QUERY=(?<query>[^&]+)[^"]+)"[^>]+>(?<title>[^<]+)<\/a> (?<tome>\d+)/i;

export const parseResult = ({ Results }) => Results.map(
  ({ FriendlyUrl: url, Resource: r, CustomResult }) => {
    const { groups: saga } = CustomResult.match(regSaga) || {};
    if (saga) {
      const queryObject = Url.parse(saga.link,true).query;
      delete saga.link;
      saga.query = queryObject.QUERY;
    }

    const isbn = (r.Id || '')
      .replace(/isbn:/, '')
      .replace(/[-]/g, '');

    const author = r.Crtr || '-';
    const title = r.Ttl;
    const uid = r.RscUid;
    const description = r.Desc;

    return {
      _sorted: [get(saga, 'title'), get(saga, 'tome'), title].join(''),
      url,
      author,
      title,
      uid,
      saga,
      isbn,
      description,
      holding: {
        id: r.RscId,
        base: r.RscBase
      }
    };
  }
).sort((a, b) => {
  const aSorted = a._sorted.toLowerCase();
  const bSorted = b._sorted.toLowerCase();

  if (aSorted < bSorted) {
    return -1;
  }

  if (aSorted > bSorted) {
    return 1;
  }

  return 0;
});

const postRequest = async ({ req, uri, method = 'POST', body }) => {
  const { session: { user = {} } } = req;

  if (!user || !user.cookies) {
    throw Error('missing auth cookies');
  }

  const request = getRequest();
  request.loadCookies(user.cookies, BASE_URL);

  return request({
    method,
    uri,
    body,
    headers: {
      'content-type': 'application/json'  // Is set automatically
    }
  }).then(res => {
    const { success, d } = res;
    if (!success) {
      throw Error('request failed');
    }

    return d;
  });
};

export const post = async (req, res) => {
  const { sendSuccess, sendError } = makeResponse(res);

  const request = getRequest(res);

  if (!request) {
    return sendError('need to login');
  }

  return postRequest({
    req,
    uri: URL_BASKET,
    body: {
      query: {
        ResultSize: 100,
        Page: 0,
        SearchInput: '',
        LabelFilter: []
      }
    }
  })
  .catch(({ message }) => {
    delete req.session.user;
    sendError(message)
  })
  .then(parseResult)
  .then(books => sendSuccess({ books }));
};

export const put = (req, res) => {
  const { sendSuccess, sendError } = makeResponse(res);
  const { body: { uid: RscUid } } = req;

  return postRequest({
    req,
    uri: URL_BASKET_ADD,
    body: {
      resIdentifier: { RscUid }
    }
  }).catch(({ message }) => sendError(message))
    .then(sendSuccess);
};

export const del = async (req, res) => {
  const { sendSuccess, sendError } = makeResponse(res);
  const { body: { uid } } = req;

  return postRequest({
    req,
    uri: URL_BASKET_DEL,
    body: { uid }
  }).catch(({ message }) => sendError(message))
    .then(sendSuccess);
};

