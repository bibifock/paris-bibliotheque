import getRequest from 'utils/request';
import makeResponse from 'utils/makeResponse';
import { URL_HOLDING, BASE_URL } from 'utils/urls';

const getHoldings = (request, RscId, Docbase) => request({
  method: 'POST',
  uri: URL_HOLDING,
  body: {
    Record: { RscId, Docbase }
  },
  headers: {
    'content-type': 'application/json'  // Is set automatically
  }
}).then(({ success, d }) => {
  if (!success) throw Error('holding request failed');

  const holdings = d.Holdings.map(
    ({ IsAvailable, IsReservable, Site, Statut, WhenBack, Cote }) => ({
      isAvailable: IsAvailable,
      isReservable: IsReservable,
      site: Site,
      status: Statut,
      when: WhenBack,
      cote: Cote
    })
  );

  holdings.sort((a, b) => a.site < b.site ? -1 : 1);

  return holdings;
});


// sample value {url>/holdings/762046-SYRACUSE.json
export const post = (req, res) => {
  const { sendSuccess, sendError } = makeResponse(res);

  const { user = {} } = req.session;

  const request = getRequest();

  if (!user || !user.cookies) {
    return sendError('need to login');
  }

  const { id, base } = req.body;

  request.loadCookies(user.cookies, BASE_URL);

  return getHoldings(request, id, base)
    .then(holding => sendSuccess({ holding }))
    .catch(({ message }) => sendError(message));
};
