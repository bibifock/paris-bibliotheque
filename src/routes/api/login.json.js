import getRequest from 'utils/request';
import makeResponse from 'utils/makeResponse';
import { URL_LOGIN, BASE_URL } from 'utils/urls';

const login = ({ request, username, password }) => request({
  method: 'POST',
  uri: URL_LOGIN,
  form: { username, password }
}).then(({ success }) => {
  return success;
});

export const post = async (req, res) => {
  const { sendSuccess, sendError } = makeResponse(res);
  const { login: username, password } = req.body;

  if (!username || !password) {
    return sendError('invalid login / password');
  }

  const request = getRequest();

  const logged = await login({ request, username, password });
  if (!logged) {
    return sendError('login failed');
  }

  req.session.user = {
    username,
    cookies: request.getCookies(BASE_URL)
  };

  sendSuccess();
};
