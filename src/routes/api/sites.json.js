import getRequest from 'utils/request';
import makeResponse from 'utils/makeResponse';
import { URL_SITES } from 'utils/urls';

const request = getRequest();

const getSites = () => request({
  method: 'POST',
  uri: URL_SITES,
  body: {
    fieldUid:456,
    term: '.*',
    scenarioCode: 'CATALOGUE'
  },
  headers: {
    'content-type': 'application/json'  // Is set automatically
  }
}).then(({ success, d }) => {
  if (!success) throw 'getSites failed';

  return d.map(
    ({ value, label }) => ({ label, value })
  );
});


export const get = async (req, res) => {
  const { sendSuccess, sendError } = makeResponse(res);

  const sites = await getSites();
  sendSuccess({ sites });
};
