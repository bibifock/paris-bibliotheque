import getRequest from 'utils/request';
import makeResponse from 'utils/makeResponse';
import { URL_SEARCH } from 'utils/urls';

import { parseResult } from './basket.json';

const request = getRequest();

const makeSearch = (search) => request({
  method: 'POST',
  uri: URL_SEARCH,
  body: {
    query: {
      FacetFilter: '{"_586":"Livre"}',
      QueryString: search,
      ScenarioCode: 'CATALOGUE'
    }
  },
  headers: {
    'content-type': 'application/json'  // Is set automatically
  }
}).then(({ success, d, errors }) => {
  if (!success) throw errors;

  return parseResult(d);
});


export const post = async (req, res) => {
  const { sendSuccess, sendError } = makeResponse(res);
  const { search } = req.body;

  try {
    const result = await makeSearch(search);
    sendSuccess({ result });
  } catch(e) {
    sendError(e);
  }
};
