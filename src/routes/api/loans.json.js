import getRequest from 'utils/request';
import makeResponse from 'utils/makeResponse';
import { URL_LOANS, BASE_URL, URL_ACTION_LOANS_RENEW } from 'utils/urls';

const getLoans = ({ request }) => request({
  method: 'GET',
  uri: URL_LOANS,
  qs: {
    serviceCode: 'SYRACUSE',
    token: Date.now(),
    userUniqueIdentifier: '',
    timestamp: Date.now()
  }
}).then(response => {
  const { success, d, errors } = response;
  if (!success) {
    throw 'getLoans failed: ' +  errors[0].msg;
  }

  return d.Loans.map(
    (loans) => ({
      renewData: {
        HoldingId: loans.HoldingId,
        RecordId: loans.RecordId,
        Id: loans.HoldingId
      },
      title: loans.Title,
      location: loans.Location,
      locationLink: loans.AdditionalProperties.LocationLink,
      thumb: loans.ThumbnailUrl,
      canRenew: loans.CanRenew,
      cannotRenewReason: loans.CannotRenewReason,
      isLate: loans.IsLate,
      isSoonLate: loans.IsSoonLate,
      status: loans.State,
      whenBack: parseInt(loans.WhenBack.replace(/\/Date\((.+)\+.+/gi, '$1'))
    })
  );
});

const renewLoans = ({ request, loans }) => request({
  method: 'POST',
  uri: URL_ACTION_LOANS_RENEW,
  body: {
    serviceCode: 'SYRACUSE',
    loans
  },
  headers: {
    'content-type': 'application/json'  // Is set automatically
  }
}).then(response => {
  const { success, d } = response;
  if (!success) {
    throw 'renew loans failed ';
  }

  const { ErrorCount, Errors } = d;

  if (ErrorCount > 0) {
    throw 'renew loans operation failed: ' + Errors[0].Value;
  }
});

export const post = async (req, res) => {
  const { sendSuccess, sendError } = makeResponse(res);

  const { user = {} } = req.session;

  const request = getRequest();

  if (!user || !user.cookies) {
    return sendError('need to login');
  }

  request.loadCookies(user.cookies, BASE_URL);

  const loans = await getLoans({ request })
    .catch(e => {
      sendError(e)
    });

  sendSuccess({ loans });
};

export const put = async (req, res) => {
  const { sendSuccess, sendError } = makeResponse(res);

  const { loans = [] } = req.body || {};

  if (!loans.length) {
    return sendError('missing loan information');
  }

  const { user = {} } = req.session;

  const request = getRequest();

  if (!user || !user.cookies) {
    return sendError('need to login');
  }


  request.loadCookies(user.cookies, BASE_URL);

  renewLoans({ request, loans })
    .then(() => sendSuccess({}))
    .catch(e => {
      sendError(e)
    });
}
