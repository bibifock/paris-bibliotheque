import { generateActions } from 'utils/testing/storybook';

import { baseProps } from 'ui/organisms/book/stories.utils';

import Component from './BooksList';

const getStory = (props = {}) => () => ({
  Component,
  props: {
    noResult: 'aucun résultat',
    ...props
  },
  on: generateActions(['basket', 'seeMore', 'sagaClick'])
});

export default {
  title: 'organisms/books-list'
};

export const base = getStory();

export const loading = getStory({ loading: true });

export const items = getStory({
  items: [baseProps, baseProps],
});

export const filtered = getStory({
  items: [baseProps, baseProps],
  filters: {
    library: baseProps.libraries[0].site
  }
});
