import { generateActions } from 'utils/testing/storybook';

import Component from './Navbar';

const storyProps = {
  Component,
  props: {
    logged: true,
    links: [
      { icon: 'search' },
      { icon: 'basket', counter: 5 }
    ],
    basket: 60,
    active: 'basket',
    libraries: Array.from(
      Array(10),
      (_, value) => ({ label: `label ${value}`, value })
    )
  },
  on: generateActions(['click', 'logout'])
};

export default {
  title: 'organisms/navbar'
};

export const base = () => storyProps;
