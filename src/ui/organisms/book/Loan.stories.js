import { getStory, baseProps } from './stories.utils';

const getLoanStory = (props = {}) => getStory({
  isLoan: true,
  whenBack: new Date(),
  canRenew: true,
  location: '75003 - Marguerite Audoux',
  locationLink: 'https://www.paris.fr/equipements/bibliotheque-marguerite-audoux-1665',
  ...props
});

export default {
  title: 'organisms/book/loan'
};

export const base = getLoanStory();
export const soonLate = getLoanStory({ isSoonLate: true });
export const late = getLoanStory({ isLate: true });
export const cannotRenew = getLoanStory({
  canRenew: false,
  cannotRenewReason: 'We can have 4 idiots and a fat guy, but no arsonist!'
});

export const filtred = getLoanStory({
  library: baseProps.libraries[0].site,
  open: true
});

export const loanError = getLoanStory({
  canRenew: false,
  error: 'Yes, you were. You were in terrible peril.'
});
