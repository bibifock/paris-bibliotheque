import { getStory, baseProps } from './stories.utils';
export default {
  title: 'organisms/book'
};

export const base = getStory();
export const loading = getStory({ open: true, loading: true });
export const withoutLibraries = getStory({ open: true, libraries: [] });
export const open = getStory({ open: true });
export const inBasket = getStory({ inBasket: true });
export const filtred = getStory({
  open: true,
  library: baseProps.libraries[0].site
});

export const FiltredByUnknownLibrary = getStory({
  open: true,
  library: 'Quand on est idiot, on plante des carottes on ne s\'occupe pas de sécurité !'
});
