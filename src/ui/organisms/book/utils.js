const getLoanIcon = ({
  isLate,
  isSoonLate,
  whenBack,
  canRenew,
  cannotRenewReason
}) => {
  let iconColor = 'success';
  if (isLate) {
    iconColor = 'danger';
  } else if (isSoonLate) {
    iconColor = 'warning';
  }

  return {
    icon: canRenew ? 'time' : null,
    iconColor,
    status: whenBack.toGMTString().replace(/ \d{2}:.+/, ''),
    detail: cannotRenewReason
  };
}

const getBasketStatus = ({ status, isAvailable, when, cote }) => ({
  statusColor: isAvailable ? 'success' : 'danger',
  status,
  detail: when || cote
});

const getBasketIcon = ({
  inBasket,
  libraries,
  library
}) => {
  let statusInfos = {};
  if (libraries && libraries.length) {
    if (library) {
      const filterSite = libraries.find(({ site }) => site === library);
      if (filterSite) {
        statusInfos = getBasketStatus(filterSite);
      }
    } else {
      statusInfos = getBasketStatus({
        isAvailable: libraries.findIndex(({ isAvailable }) => isAvailable) > -1
      });
    }
  }

  return ({
    icon: 'basket',
    iconColor: inBasket ? 'danger' : 'primary',
    ...statusInfos
  });
}

export const getStatus = (book = {}) => {
  const {
    isLoan,
    loading,
    error
  } = book;

  const status = {
    loading,
    ...(isLoan ? getLoanIcon(book) : getBasketIcon(book))
  };

  if (error) {
    status.detail = error;
    status.detailColor = 'warning';
  }
  return status;
};
