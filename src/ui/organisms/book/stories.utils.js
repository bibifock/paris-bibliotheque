import { generateActions } from 'utils/testing/storybook';

import Component from './Book';

export const baseProps = {
  url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/1160014/le-testament-d-involution',
  author: 'Fabien Cerutti. Auteur',
  title: 'Le testament d\'involution',
  saga: {
    link: '#test',
    title: 'Le bâtard de Kosigan',
    tome: '4'
  },
  isbn: '9782354086466',
  description: 'En 1341, une prophétie promet à quiconque l accomplira d obtenir le pouvoir de plusieurs anciens dieux. Le bâtard de Kosigan est chargé de découvrir le lieu du rituel. En 1900, Elisabeth Hardy, ex-fiancée de Kergaël de Kosigan, invite ses pires ennemis à son mariage pour leur faire boire un élixir d oubli. ©Electre 2018<br/><br/>',
  libraries: [{
    isAvailable: false,
    isReservable: false,
    site: '75001 - Canopée la fontaine',
    status: 'Emprunté',
    when: '01/09/2020'
  }, {
    isAvailable: false,
    isReservable: false,
    site: '75005 - Rainer Maria Rilke',
    status: 'Emprunté',
    when: '04/09/2020'
  }, {
    isAvailable: true,
    isReservable: false,
    site: '75008 - Europe',
    status: 'En rayon',
    when: null
  }, {
    isAvailable: true,
    isReservable: false,
    site: '75009 - Drouot',
    status: 'En rayon',
    when: null
  }],
  holding: {
    id: '1160014',
    base: 'SYRACUSE'
  }
};

export const getStory = (props = {}) => () => ({
  Component,
  on: generateActions(['basket', 'seeMore', 'sagaClick']),
  props: {
    ...baseProps,
    ...props
  }
});
