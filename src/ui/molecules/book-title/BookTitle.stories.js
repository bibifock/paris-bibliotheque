import { generateActions } from 'utils/testing/storybook';

import Component from './BookTitle';

const getStory = (props = {}) => () => ({
  Component,
  props: {
    url: 'https://bibliotheques.paris.fr/Default/doc/SYRACUSE/1160014/le-testament-d-involution',
    author: 'Fabien Cerutti. Auteur',
    title: 'Le testament d\'involution',
    isbn: '9782354086466',
    ...props
  },
  on: generateActions(['sagaClick'])
});

export default {
  title: 'molecules/book-title'
};

export const base = getStory();
export const saga = getStory({
  saga: {
    link: '#test',
    title: 'Le bâtard de Kosigan',
    tome: '4'
  }
});

export const loan = getStory({
  location: '75003 - Marguerite Audoux',
  locationLink: 'https://www.paris.fr/equipements/bibliotheque-marguerite-audoux-1665',
});
