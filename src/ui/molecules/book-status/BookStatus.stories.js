import { generateActions } from 'utils/testing/storybook';
import Component from './BookStatus';

const getStory = (props = {}) => () => ({
  Component,
  props,
  on: generateActions(['click'])
});

export default {
  title: 'molecules/book-status'
};

export const base = getStory();
export const custom = getStory({
  status: 'Je suis prêt à tout pour échouer. Et en plus, j\'ai une mallette.',
  statusColor: 'warning',
  icon: 'time',
  iconColor: 'danger',
  detail: 'Achète des cotons-tiges alors, il te demande pour la deuxième et la dernière fois si tu peux pas sortir pour téléphoner à ta pouffiasse !',
  detailColor: 'primary'
});

export const loading = getStory({
  loading: true
});
