import { generateActions } from 'utils/testing/storybook';

import Component from './NavIconStories';

const storyProps = {
  Component,
  props: {
    icon: 'search',
    counter: 3
  },
  on: generateActions(['click'])
};

export default {
  title: 'molecules/nav-icon'
};

export const base = () => storyProps;
