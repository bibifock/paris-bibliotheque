import Component from './IconStories.svelte';

const getStory = () => ({
  Component
});

export default {
  title: 'atoms/icon'
};

export const all = () => getStory();
