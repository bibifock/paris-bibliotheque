import { generateActions } from 'utils/testing/storybook';
import Component from './SectionStories';

const getStory = (props = {}) => () => ({
  Component,
  props: {
    name: 'section title',
    total: 'total label',
    ...props
  },
  on: generateActions(['click'])
});

export default {
  title: 'atoms/section'
};

export const base = getStory();

export const open = getStory({ open: true });
