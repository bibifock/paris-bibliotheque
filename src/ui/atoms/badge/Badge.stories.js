import Component from './BadgeStories';

const storyProps = {
  Component
};

export default {
  title: 'atoms/badge'
};

export const base = () => storyProps;
