import { generateActions } from 'utils/testing/storybook';

import Component from './TextField.svelte';

const on = generateActions(['change']);

export default {
  title: 'atoms/fields/text'
};

export const base = () => ({
  Component,
  props: {
    label: 'label',
    value: 'value'
  },
  on
});

export const small = () => ({
  Component,
  props: {
    label: 'label',
    value: 'value',
    small: true
  },
  on
});

export const number = () => ({
  Component,
  props: {
    label: 'label',
    value: 10,
    type: 'number'
  },
  on
});

export const error = () => ({
  Component,
  props: {
    label: 'label',
    value: 'value',
    error: 'error message'
  },
  on
});
