import Component from './ImgIsbnStories';

const storyProps = {
  Component
};

export default {
  title: 'atoms/img-isbn'
};

export const base = () => storyProps;

export const validIsbn = () => ({
  ...storyProps,
  props: {
    isbn: '9782354086466'
  }
});

export const invalidIsbn = () => ({
  ...storyProps,
  props: {
    isbn: '12345'
  }
});
