import Component from './IconButtonStories';

const storyProps = {
  Component
};

export default {
  title: 'atoms/icon-button'
};

export const base = () => storyProps;
