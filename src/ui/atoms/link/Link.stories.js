import Component from './LinkStories';

const getStory = (props = {}) => () => ({
  Component,
  props: {
    href: '#test',
    ...props
  }
});

export default {
  title: 'atoms/link'
};

export const base = getStory();
export const blank = getStory({ target: '_blank' });
export const noHref = getStory({ href: undefined });
