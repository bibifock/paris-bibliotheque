import Component from './StatusStories';

const storyProps = {
  Component
};

export default {
  title: 'atoms/status'
};

export const base = () => storyProps;
