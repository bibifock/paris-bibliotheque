import Component from './Loading';

const getStory = (props = {}) => () => ({
  Component,
  props
});

export default {
  title: 'atoms/loading'
};

export const base = getStory();
export const small = getStory({ small: true });
