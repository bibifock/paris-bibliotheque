import { generateActions } from 'utils/testing/storybook';

import Component from './LayoutStories';

const getStory = (props = {}) => () => ({
  Component,
  props: {
    active: 'basket',
    links: [
      { icon: 'search' },
      { icon: 'basket', counter: 60 }
    ],
    logged: true,
    libraries: Array.from(
      Array(10),
      (_, value) => ({ label: `label ${value}`, value })
    )
  },
  on: generateActions(['click', 'logout'])
});

export default {
  title: 'pages/layout'
};

export const base = getStory();
