import { generateActions } from 'utils/testing/storybook';

import { baseProps } from 'ui/organisms/book/stories.utils';

import Component from './Search';

const getStory = (props = {}) => () => ({
  Component,
  props: {
    noResult: 'aucun livre trouvé',
    placeholder: 'tapez votre recherche',
    ...props
  },
  on: generateActions(['search', 'select', 'seeMore', 'sagaClick'])
});

export default {
  title: 'pages/search'
};

export const base = getStory();

export const loading = getStory({ loading: true, search: 'my book' });

export const noResult = getStory({ items: [] });

export const withResult = getStory({
  items: [ baseProps, baseProps ]
});
