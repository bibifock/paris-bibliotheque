import { generateActions } from 'utils/testing/storybook';

import Component from './Login';

const getStory = (props = {}) => () => ({
  Component,
  props,
  on: generateActions(['submit'])
});

export default {
  title: 'pages/login'
};

export const base = getStory();
export const loading = getStory({ loading: true });
export const error = getStory({ error: 'ceci est une erreur' });
