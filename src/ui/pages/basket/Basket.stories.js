import { generateActions } from 'utils/testing/storybook';
import { baseProps } from 'ui/organisms/book/stories.utils';

import Component from './BasketStories';

const getLoan = (props = {}) => ({
  ...baseProps,
  isLoan: true,
  whenBack: new Date(),
  canRenew: true,
  ...props
});

const basket = {
  ...baseProps,
  inBasket: true
};

const getStory = (props = {}) => () => ({
  Component,
  props: {
    loans: [
      getLoan(),
      getLoan({ isSoonLate: true }),
      getLoan({ isLate: true, cannotRenewReason: 'because you are ugly' })
    ],
    baskets: [ basket, { ...basket, isbn: '9782915830224', title: 'another' } ],
    ...props
  },
  on: generateActions([
    'loanExtension',
    'basketExclude',
    'open',
    'sagaClick'
  ])
});

export default {
  title: 'pages/basket'
};

export const base = getStory();

export const loading = getStory({
  isLoanLoading: true,
  isBasketLoading: true
});

export const filtred = getStory({
  library: baseProps.libraries[0].site
});
