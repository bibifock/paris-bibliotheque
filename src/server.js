import sirv from 'sirv';
import express from  'express';
import bodyParser from 'body-parser';
import compression from 'compression';
import * as sapper from '@sapper/server';
import session from 'express-session';
import sessionFileStore from 'session-file-store';

const FileStore = sessionFileStore(session);

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

const app = express() // You can also use Express
  .use(session({
    secret: process.env.APP_SECRET || '123',
    resave: false,
    saveUninitialized: true,
    cookie: {
      maxAge: 31536000
    },
    store: new FileStore({
      path: process.env.NOW ? '/tmp/sessions' : '.sessions'
    })
  }))
  .use(
    compression({ threshold: 0 }),
    sirv('static', { dev }),
    bodyParser.json(),
    sapper.middleware({
      session: req => ({
        user: req.session && req.session.user
      })
    })
  )
  .listen(PORT, err => {
    if (err) console.log('error', err);
  });

const handleExit = (signal) => {
  console.log(`Received ${signal}. Close my server properly.`)
  app.close(function () {
    process.exit(0);
  });
}

process.on('SIGINT', handleExit);
process.on('SIGQUIT', handleExit);
process.on('SIGTERM', handleExit);
